import { Builder, By, until } from "selenium-webdriver";

function openGoogleAndSearchWebDriver() {
    driver.get("http://www.google.com/ncr");

    driver.findElement(By.name("q")).sendKeys("webdriver");
    driver.findElement(By.name("btnK")).click();
    driver.wait(until.titleIs("webdriver - Google Search"), 1000);

    driver.executeScript(() => {
        scroll(0, 100);
    });

    driver.sleep(5000);
    driver.quit();
}

function openWebSnifferNet() {
    driver.get("http://websniffer.cc/");

    const selectButton = driver.findElement(By.name("useragent"));
    selectButton.click();
    var option = selectButton.findElement(By.xpath("./option[text()=\"your user agent\"]"));
    option.click();

    driver.findElement(By.name("url")).sendKeys("www.google.com/ncr");
    driver.findElement(By.name("submit")).click();
}


const driver = new Builder()
    .forBrowser("firefox")
    .build();

// openGoogleAndSearchWebDriver()
openWebSnifferNet();