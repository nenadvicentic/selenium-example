# Navigate web pages with Selenium driver and NodeJS  #


## Installation of dependencies ##

1. Install Node version 8.9+.
2. Install TypeScript globally: npm i -g typescript
3. Install [mozilla/geckodriver](https://github.com/mozilla/geckodriver/releases) and make sure executable is on the PATH.
4. Restore local project dependencies into `node_modules` folder: npm install.

## Modifying and running example code ##

4. Edit `index.ts` file, when done run `tsc` in the console to transpile TypeScript into `dist/index.js` file.
5. Run `npm start` in console.
